$(document).ready(function (e) {
	$("#gameforms").on('submit',(function(e) {
		e.preventDefault();
		$('html,body').animate({scrollTop: $("#wrapgame").offset().top},'slow');
		$('#wrapgame').html('<div class="load"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i><br> โปรดรอสักครู่</div>');

		$.ajax({
		url: URL+"action/addgame",
		type: "POST",
		data: new FormData(this),
		contentType: false,      
		cache: true,            
		processData:false, 
		success: function(data)
		{

			if(data.trim() != "fail"){

			$('#wrapgame').html(data);

			$('#urlsteam').val('');

			}else{

			$('#wrapgame').html('<div class="load text-danger"><i class="fa fa-times"></i> ไม่สามารถซื้อเกมส์นี้ได้</div>');

			$('#urlsteam').val('');

			}



		}
		});
   }));

 });


function addcart(appid,type){

		var data={'appid':appid,'type':type};

		$('#cart_game').html('<div class="load"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i><br> โปรดรอสักครู่</div>');


		$.ajax({
		url: URL+"action/addcart",
		type: "POST",
		data: data,
		success: function(data)
		{

			// alert(data)

			if(data.trim() != "fail"){

			$('#cart_game').html(data);

			$('#btn_'+appid).attr('disabled','disabled').html('<i class="fa fa-check" aria-hidden="true"></i> เลือกแล้ว');

			}else{

			return false;

			}



		}

		});


}


function removecart(appid){

		var data={'appid':appid};


		// alert(appid)

		$('#cart_game').html('<div class="load"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i><br> โปรดรอสักครู่</div>');


		$.ajax({
		url: URL+"action/removecart",
		type: "POST",
		data: data,
		success: function(data)
		{

			// alert(data)

			if(data.trim() != "fail"){

			$('#cart_game').html(data);

			$('#btn_'+appid).removeAttr('disabled').html('หยิบลงตะกร้า');

			}else{

			return false;

			}



		}

		});


}

function checktrue(oid){

		var data={'oid':oid};

		$.ajax({
		url: URL+"pay/checktrue",
		type: "POST",
		data: data,
		success: function(data)
		{
		  $('#list-true').html(data);
		  truesum(oid)
		}

	});


}


function truesum(oid){

		var data={'oid':oid};

		$.ajax({
		url: URL+"pay/sumtrue",
		type: "POST",
		data: data,
		success: function(data)
		{

			$('#addtrue').html(data);

		}

		});


}