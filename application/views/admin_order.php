<?php
 include "section/header.php";
?>


<script type="text/javascript">
    
    $(function() {
      $('.datatable').DataTable( {
        "order": [[ 0, "desc" ]]
      });
    });


</script>

        <div class="row">
            <div class="col-sm-12">

               <div class="col-sm-2" style="margin-bottom:25px;">

          <?php
           include "section/left-menu.php";
          ?>


               </div>

              <div class="col-sm-10" style="margin-bottom:25px;">
	              <h2><i class="fa fa-steam-square"></i> จัดการสั่งซื้อ</h2>                

                               <div class="table-respons">
                                    <table id="example" class="datatable table table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th class="text-center">เลขอ้างอิง</th>
                                              <th class="text-center">อีเมล์</th>
                                              <th class="text-center">วันที่สั่ง</th>
                                              <th class="text-center">หมดอายุ</th>
                                              <th class="text-center">สถานะ</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php

                                           for ($i=0; $i < count($so) ; $i++) {

                                         ?>
                                            <tr>
                                              <td class="text-center"><a href="<?php echo $config['base_url'];?>order/detail/<?php echo $so[$i]['OrderId'];?>"><?php echo $so[$i]['OrderId'];?></a></td> 
                                              <td class="text-center"><?php echo $so[$i]['OrderEmail'];?></td>
                                              <td class="text-center"><?php echo $so[$i]['OrderDate'];?></td>
                                              <td class="text-center"><?php echo $so[$i]['OrderExpire'];?></td>
                                              <td class="text-center">

                                              <?php
                                                if($so[$i]['OrderStatus']==1){
                                              ?>

                                              <span class="text-danger">สั่งซื้อ</span>

                                              <?php }else if($so[$i]['OrderStatus']==2){?>

                                              <span class="text-warning">ชำระเงินแล้ว</span>

                                              <?php }else if($so[$i]['OrderStatus']==3){?>

                                              <span class="text-success">สำเร็จ</span>

                                              <?php }else{?>

                                              <span class="text-info">หมดอายุ</span>

                                              <?php } ?>

                                              </td>
                                            </tr>

                                         <?php } ?>

                                        </tbody>
                                     </table>

                                    </div>


            </div>
           
        </div>


    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/js/index.js"></script>
    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/dataTables.bootstrap.min.js"></script>
