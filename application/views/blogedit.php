<?php
include "section/header.php";
?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2" style="margin-bottom:25px;">
			<?php
			include "section/left-menu.php";
			?>
		</div>
		<div class="col-sm-10" style="margin-bottom:25px;">
			<h2><i class="fa fa-steam-square"></i> แก้ไขหน้าเว็บ</h2>
			<form class="form-horizontal" method="post" action="<?php echo $config['base_url'].'bloglist/edit/'.$idedit;?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อหน้า</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" placeholder="ชื่อหน้า" value="<?php echo $b['name'];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">ซ้อน/แสดง</label>
							<div class="col-sm-10">
								<select class="form-control" name="status">
									<option <?php echo ($b['status'] == '1') ? 'selected=""' : '' ;?> value="1">แสดง</option>
									<option <?php echo ($b['status'] == '0') ? 'selected=""' : '' ;?> value="0">ซ้อน</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">แสดงที่</label>
							<div class="col-sm-10">
								<select class="form-control" name="showon">
									<option <?php echo ($b['showon'] == '0') ? 'selected=""' : '' ;?> value="0">ทั้งคู่</option>
									<option <?php echo ($b['showon'] == '1') ? 'selected=""' : '' ;?> value="1">ด้านบน</option>
									<option <?php echo ($b['showon'] == '2') ? 'selected=""' : '' ;?> value="2">ด้านข้าง</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">รายละเอียด</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="description" placeholder="รายละเอียด"><?php echo $b['description'];?></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							<input type="hidden" name="action" value="save">
								<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
								<button type="submit" class="btn btn-primary">แก้ไข</button>
							</div>
						</div>
					</form>
		</div>
	</div>

	<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
	<script>
		CKEDITOR.replace('description');
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>