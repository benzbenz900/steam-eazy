<?php
 include "section/header.php";
?>


        <div class="row">
            <div class="col-sm-12">

            	 <div class="col-sm-2" style="margin-bottom:25px;">

					<?php
					 include "section/left-menu.php";
					?>


            	 </div>

	            <div class="col-sm-10" style="margin-bottom:25px;">
	              <h2><i class="fa fa-steam-square"></i> แก้ไขข้อมูลเว็บไซต์</h2>                

					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'];?>setting/edit" enctype="multipart/form-data">

					  <div class="form-group">
					    <label class="col-sm-2 control-label"></label>
					    <div class="col-sm-10">


							<?php
								if(isset($_SESSION['process'])){
							?>

							<div class="alert alert-success alert-dismissible" role="alert">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							  <strong>สำเร็จ!</strong> แก้ไขข้อมูลเว็บไซต์เรียบร้อยแล้ว
							</div>

							<?php
							   
							   unset($_SESSION['process']);

								}
							?>


					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">ชื่อเว็บไชต์</label>
					    <div class="col-sm-10">
					      <input type="text" name="nameweb" class="form-control" value="<?php echo $s['nameweb'];?>" placeholder="ชื่อเว็บไชต์">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">เปิดปิดเว็บ</label>
					    <div class="col-sm-10">
					      <select class="form-control" name="openweb">
					         <option <?php echo ($s['openweb'] == '1') ? 'selected=""' : '' ;?> value="1">เปิดเว็บ</option>
					         <option <?php echo ($s['openweb'] == '0') ? 'selected=""' : '' ;?> value="0">ปิดเว็บ</option>
					      </select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">ชื่อผู้ใช้</label>
					    <div class="col-sm-10">
					      <input type="text" name="username" class="form-control" value="<?php echo $s['username'];?>" placeholder="ชื่อผู้ใช้">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">รหัสผ่าน</label>
					    <div class="col-sm-10">
					      <input type="password" name="password" class="form-control" value="<?php echo $s['password'];?>" placeholder="รหัสผ่าน">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">Logo</label>
					    <div class="col-sm-10">
					      <input type="file" name="logo">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">BG Image</label>
					    <div class="col-sm-10">
					      <input type="file" name="bg">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">Email</label>
					    <div class="col-sm-10">
					      <input type="text" name="email" class="form-control" value="<?php echo $s['email'];?>" placeholder="Email">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">TmTopUp UID</label>
					    <div class="col-sm-10">
					      <input type="text" name="tmuid" class="form-control" value="<?php echo $s['tmuid'];?>" placeholder="TmTopUp UID">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">TmTopUp Key</label>
					    <div class="col-sm-10">
					      <input type="text" name="tmkey" class="form-control" value="<?php echo $s['tmkey'];?>" placeholder="TmTopUp Key">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">FB appid</label>
					    <div class="col-sm-10">
					      <input type="text" name="FBappid" class="form-control" value="<?php echo $s['FBappid'];?>" placeholder="FB appid">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">เบอร์ติดต่อ</label>
					    <div class="col-sm-10">
					      <input type="text" name="phone" class="form-control" value="<?php echo $s['phone'];?>" placeholder="เบอร์ติดต่อ">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">รายละเอียด</label>
					    <div class="col-sm-10">
					      <textarea class="form-control editor" name="detail"><?php echo $s['detail'];?></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">ฟุตเตอร์</label>
					    <div class="col-sm-10">
					      <textarea class="form-control editor" name="footer"><?php echo $s['footer'];?></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-success">บันทึก</button>
					    </div>
					  </div>
					</form>

            </div>
           
        </div>





<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
<script>
  CKEDITOR.replaceAll('editor');
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=<?php echo $s['appid'];?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>

</html>