<?php
include "section/header.php";
?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2" style="margin-bottom:25px;">
			<?php
			include "section/left-menu.php";
			?>
		</div>
		<div class="col-sm-10" style="margin-bottom:25px;">
			<h2><i class="fa fa-steam-square"></i> เพิ่มสไลต์</h2>
			<button class="btn btn-success" data-toggle="modal" data-target="#myModal">เพิ่มสไลต์</button>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>ชื่อเสไลต์</th>
						<th>link</th>
						<th>ลบ</th>
					</tr>
				</thead>
				<tbody>
					<?php
					for ($i=0; $i < count($steam_slides); $i++) {
						echo '
						<tr>
							<th scope="row">'.$steam_slides[$i]['id'].'</th>
							<td>'.$steam_slides[$i]['name'].'</td>
							<td>'.$steam_slides[$i]['link'].'</td>
							<td><a class="btn btn-info btn-xs" href="'.$config['base_url'].'slides/edit/'.$steam_slides[$i]['id'].'" role="button">แก้ไข</a> <a class="btn btn-danger btn-xs" href="'.$config['base_url'].'slides/del/'.$steam_slides[$i]['id'].'" role="button">ลบ</a></td>
						</tr>
						';
					}
					?>
				</tbody>
			</table>
		</div>
	</div>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">เพิ่มสไลต์</h4>
				</div>
				<div class="modal-body">

					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'].'slides/add';?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อเสไลต์</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" placeholder="ชื่อเสไลต์">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">รายละเอียด</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="detail" placeholder="รายละเอียด">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Link รูป</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linkimage" placeholder="Link รูป">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Link Web</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="link" placeholder="Link Web">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
								<button type="submit" class="btn btn-primary">เพิ่ม</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>