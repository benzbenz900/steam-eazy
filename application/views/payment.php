<?php
 include "section/header.php";
?>


        <div class="row">
            <div class="col-sm-12">

            	 <div class="col-sm-2" style="margin-bottom:25px;">

					<?php
					 include "section/left-menu.php";
					?>


            	 </div>

	            <div class="col-sm-10" style="margin-bottom:25px;">
	              <h2><i class="fa fa-steam-square"></i> แก้ไขบัญชีธนาคาร</h2>                

					  <div class="form-group">
					    <div class="col-sm-12">


							<?php
								if(isset($_SESSION['process'])){
							?>

							<div class="alert alert-success alert-dismissible" role="alert">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							  <strong>สำเร็จ!</strong> แก้ไขบัญชีธนาคารเรียบร้อยแล้ว
							</div>

							<?php
							   
							   unset($_SESSION['process']);

								}
							?>


					    </div>
					  </div>




                    <?php

                       for ($i=0; $i < count($sp) ; $i++) {

                     ?>

					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'];?>setting/editpayment">
                     <h3>ธนาคาร <?php echo $i+1;?></h3>
                     <input type="hidden" value="<?php echo $sp[$i]['id'];?>" name="id">
					  <div class="form-group">
					    <label class="col-sm-2 control-label">ชื่อธนาคาร</label>
					    <div class="col-sm-10">
					      <input type="text" name="bank" class="form-control" value="<?php echo $sp[$i]['bankname'];?>" placeholder="ชื่อธนาคาร">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">ชื่อบัญชี</label>
					    <div class="col-sm-10">
					      <input type="text" name="name" class="form-control" value="<?php echo $sp[$i]['name'];?>" placeholder="ชื่อบัญชี">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">เลขบัญชี</label>
					    <div class="col-sm-10">
					      <input type="text" name="no" class="form-control" value="<?php echo $sp[$i]['no'];?>" placeholder="เลขบัญชี">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">สาขา</label>
					    <div class="col-sm-10">
					      <input type="text" name="branch" class="form-control" value="<?php echo $sp[$i]['brance'];?>" placeholder="สาขา">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">ประเภท</label>
					    <div class="col-sm-10">
					      <input type="text" name="type" class="form-control" value="<?php echo $sp[$i]['type'];?>" placeholder="ประเภท">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">สถานะ</label>
					    <div class="col-sm-10">
					      <select class="form-control" name="open">
					         <option <?php echo ($sp[$i]['status'] == '1') ? 'selected=""' : '' ;?> value="1">เปิดใช้งาน</option>
					         <option <?php echo ($sp[$i]['status'] == '0') ? 'selected=""' : '' ;?> value="0">ปิดใช้งาน</option>
					      </select>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-success">บันทึก</button>
					    </div>
					  </div>
					</form><hr>

					<?php } ?>

            </div>
           
        </div>





	<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
	<script>
	  CKEDITOR.replaceAll('editor');
	</script>

    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/js/index.js"></script>
    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/bootstrap.min.js"></script>
