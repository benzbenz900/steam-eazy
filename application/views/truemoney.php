<?php
 include "section/header.php";
?>

<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>application/plugins/datepicker/bootstrap-datetimepicker.css">

        <div class="row">
            <div class="col-sm-8">
             <div class="row" style="margin:0;">
	            <div class="col-sm-12" style="margin-bottom:25px;">
	              <h2><i class="fa fa-steam-square"></i> ชำระผ่านทรูมันนี่</h2>
	              <p><img src="https://4.bp.blogspot.com/-fOl-kWSve18/WF0_FRz31mI/AAAAAAAAB6Y/-vr2eIsEJ9Iwu0cyeKMTUQj4qLSOM5lbACLcB/s1600/52abee16559bd6a754aabb14c6927e7f.png" width="179" height="76" /></p>                
	            </div>
	              <div class="col-sm-3">
	                <h4>จำนวนเงิน</h4>
	              </div>
	              <div class="col-sm-9">
	                <h1><span id="addtrue"><?php echo ($sum !='' ? $sum:'0');?></span>/<?php echo $so['OrderTotal'];?> บาท</h1>              	
	              </div>
	              <br>
	               <div class="col-sm-3">
	                <h4>เวลาหมดอายุ</h4>
	               </div>
	                <div class="col-sm-9">

             <?php 

             if($so['OrderStatus']=='1'){

             ?>


				<script type="text/javascript">  

				function countDown(){  
				    var timeA = new Date(); // วันเวลาปัจจุบัน  
				    var timeB = new Date("<?php echo $so['OrderExpire'];?>"); // วันเวลาสิ้นสุด รูปแบบ เดือน/วัน/ปี ชั่วโมง:นาที:วินาที  
				    var timeDifference = timeB.getTime()-timeA.getTime();      
				    if(timeDifference>=0){  
				        timeDifference=timeDifference/1000;  
				        timeDifference=Math.floor(timeDifference);  
				        var wan=Math.floor(timeDifference/86400);  
				        var l_wan=timeDifference%86400;  
				        var hour=Math.floor(l_wan/3600);  
				        var l_hour=l_wan%3600;  
				        var minute=Math.floor(l_hour/60);  
				        var second=l_hour%60;  
				        var showPart=document.getElementById('showRemain');  
				        showPart.innerHTML=hour+ ":" +minute+ ":" +second;   
				            if(wan==0 && hour==0 && minute==0 && second==0){  
				                clearInterval(iCountDown);
				            }  
				    }  
				}  
				// การเรียกใช้  
				var iCountDown=setInterval("countDown()",1000);   

				</script>  

	                <h1><a href="#" id="showRemain"></a></h1>     

	               <?php
	                 }else{
	               ?>


	               <h4><a href="#">แจ้งชำระเงินแล้ว</a></h4>  

	               <?php
	                 }
	               ?>



	              </div>
              </div>
             <hr>
              <div class="col-sm-12">

             <?php 

             if($so['OrderStatus']=='2'){
             
              echo '<div class="text-center text-success"><h3>ทำรายการเรียบร้อยแล้ว </h3><h3>หลังจากตรวจสอบท่านจะได้รับ GAME GIFT ภายใน 10 - 20 นาที</h3></div></div>';

             }else{

             ?>


              <?php

                if($sum < $so['OrderTotal']){

              ?>


              	<form id="form-truemoney">
				  <div class="form-group">
				    <label for="exampleInputEmail1">โปรดกรอกรหัสทรูมันนี่ 14 หลัก</label>
				    <input type="text" class="form-control" name="tmn_password" id="tmn_password" maxlength="14" placeholder="โปรดกรอกรหัสทรูมันนี่ 14 หลัก">
					<input type="hidden" name="ref1" type="text" id="ref1" value="<?php echo $so['OrderEmail'];?>" />
					<input type="hidden" name="ref2" type="text" id="ref2" value="<?php echo $so['OrderId'];?>" />
					<input type="hidden" name="ref3" type="text" id="ref3" value="<?php echo $so['OrderId'];?>" />
				  </div>
				  <div class="text-right">
				  	<button type="button" class="btn btn-success" onclick="submit_tmnc()">ตกลง</button>
				  </div>
				</form>

				<?php

				  }else{

                ?>


              	<form action="<?php echo $config['base_url'];?>pay/confirmtrue" method="POST">
              	<input type="hidden" name="sid" value="<?php echo session_id();?>">
              	<input type="hidden" name="oid" value="<?php echo $so['OrderId'];?>">
              	<input type="hidden" name="email" value="<?php echo $so['OrderEmail'];?>">
              	<input type="hidden" name="total" value="<?php echo $sum;?>">
				  <div class="form-group">
				    <label for="exampleInputEmail1">ชื่อติดต่อ</label>
				    <input type="text" class="form-control"  placeholder="ชื่อผู้โอน" name="name" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputEmail1">เบอร์ติดต่อ</label>
				    <input type="text" class="form-control"  placeholder="เบอร์ติดต่อ" name="telephone" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">วัน-เวลาชำระ</label>
				     <input type="text" class="form-control" name="date" value="<?php echo date("Y-m-d H:i:s");?>" required>
				  </div>
				  <div class="text-right">
				  	<button type="submit" class="btn btn-success">ตกลง</button>
				  </div>
				  
				</form>



                <?php
				  }

				?>

              </div>
              <div class="col-sm-12">
                <p>ราคาบัตรรวมแล้วต้องไม่น้อยกว่าจำนวนเงินที่ต้องจ่าย</p>
              	<ul id="list-true" class="list-true">



              	</ul>
              </div>

              <?php } ?>

            </div>
            
           <div class="col-sm-4 wrap-cart">
                <div class="text-center">
                	<h4><strong>ข้อมูลสั่งซื้อ</strong></h4>
                </div>
               <div class="wrap-media-cart">
                	<p>อีเมล์รับ Game Gift : <?php echo $so['OrderEmail'];?></p>
                	<p>เลขอ้างอิง : <?php echo $so['OrderId'];?></p>
                	<p>วันที่สั่ง : <?php echo $so['OrderDate'];?></p>
                	<p>หมดอายุ : <?php echo $so['OrderExpire'];?></p>
                </div>

                <?php
                 if($so['OrderStatus']=='1'){
                ?>
				<form action="<?php echo $config['base_url'];?>pay/cancel" method="POST">
					<input type="hidden" name="sid" value="<?php echo session_id();?>">	
					  <input type="hidden" name="oid" value="<?php echo $so['OrderId'];?>">
	                <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิกการสั่งซื้อ</button>
	                </form>
                <?php
                 }
                ?>

                <br>
                <div class="text-center">
                	<h4><strong>รายการสั่งซื้อ</strong></h4>
                </div>
                <div class="wrap-media-cart">

	             <?php
	              for ($i=0; $i < count($sod) ; $i++) { 
	             ?>

					<div class="media media-game">
					  <div class="media-left">
					    <a href="<?php echo $sod[$i]['SteamLink'];?>">
					      <img class="media-object" src="<?php echo $sod[$i]['SteamImage'];?>" width="80">
					    </a>
					  </div>
					  <div class="media-body">
					    <p class="media-heading"><a href="<?php echo $sod[$i]['SteamLink']?>"><?php echo($sod[$i]['SteamType']=='dlc'?'[DLC]':'');?> <?php echo $sod[$i]['SteamName'];?></a></p>

					    <p><?php echo $sod[$i]['SteamPrice'];?> THB</p>

					  </div>
					</div>

				<?php
				 }
				?>
					<div class="media media-game">
					  <div class="media-left">
					    <a href="#">
					      <img class="media-object" src="assets/img/steam.png" width="65">
					    </a>
					  </div>
					  <div class="media-body">
					    <p class="media-heading"><a href="#">ค่าบริการ</a></p>
					    <p>+14.9% สำหรับบัตรทรูมันนี่</p>
					  </div>
					</div>
	            </div>
            </div>
        </div>

       	
       </div>


<script type="text/javascript" src='https://www.tmtopup.com/topup/3rdTopup.php?uid=<?php echo $tmuid;?>'></script>

<script type="text/javascript">
	
	setInterval(function(){ checktrue('<?php echo $so['OrderId'];?>') }, 1000);


</script>

<?php
 include "section/footer.php";
?>