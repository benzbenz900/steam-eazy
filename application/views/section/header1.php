<?php
session_start();
global $config;
?>
<!DOCTYPE html>
<html>
<head>
  <title><?php echo $title;?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <!-- Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
  <!-- CSS Libs -->
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/lib/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/lib/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/lib/css/animate.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/lib/css/bootstrap-switch.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/lib/css/dataTables.bootstrap.css">
  <!-- CSS App -->
  <link rel="icon" href="<?php echo $config['base_url'];?>assets/favicon.png" sizes="16x16" type="image/png">
  <link rel="stylesheet" type="text/css" href="<?php echo $config['base_url'];?>assets/css/style.css">
  <script type="text/javascript" src='<?php echo $config['base_url'];?>application/plugins/datepicker/datepickerbootstrap-datetimepicker.js'></script>
  <script type="text/javascript" src='<?php echo $config['base_url'];?>application/plugins/datepicker/datepickermoment-with-locales.js'></script>
  <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/jquery.min.js"></script>
  <script type="text/javascript">
    var URL = "<?php echo $config['base_url'];?>";
  </script>
</head>
<body style="background-image: url('<?php echo $config['base_url'].$s['bgimage'];?>');">
  <div class="container-fluid">
    <div class="container wrap-main">
      <div class="row">
        <div class="col-sm-6">
         <div class="st-logo">
           <img src="<?php echo $config['base_url'].$s['logoweb'];?>" >
         </div>
       </div>
       <div class="col-sm-6">
         <div class="st-head-name">
          <?php
          if(strpos($s['nameweb'],'http')  !== false){
            echo '<img class="img-responsive" src="'.$s['nameweb'].'">';
          }else{
            echo '<h2>'.$s['nameweb'].'</h2>';
          }
          ?>
        </div>
      </div>
    </div>
    <div class="st-wrap-main">
      <nav>
        <ul>
          <?php
          $steam_blog = ORM::for_table('steam_blog')->where('status','1')->find_many();
          ?>
          <li class="<?php echo ($page==''? 'active':'');?>"><a href="<?php echo $config['base_url'];?>">หน้าแรก</a></li>
          <?php
          for ($i=0; $i < count($steam_blog); $i++) {
            $active = ($page == 'blogview' && $id == $steam_blog[$i]['id']) ? 'active' : '';
            if($steam_blog[$i]['showon'] == '1' || $steam_blog[$i]['showon'] == '0'){
            echo '<li class="'.$active.'"><a href="'.$config['base_url'].'blogview/index/'.$steam_blog[$i]['id'].'">'.$steam_blog[$i]['name'].'</a></li>';
            }
          }
          ?>
          <li class="<?php echo ($page=='service'? 'active':'');?>"><a href="<?php echo $config['base_url'];?>service">ค่าบริการ</a></li>
          <li class="<?php echo ($page=='conditions'? 'active':'');?>"><a href="<?php echo $config['base_url'];?>conditions">คำถามที่พบบ่อย</a></li>
          <li class="<?php echo ($page=='contact'? 'active':'');?>"><a href="<?php echo $config['base_url'];?>contact">ติดต่อเรา</a></li>
          <?php
          if(!empty($_SESSION['admin'])){
           ?>
           <li class="active"><a href="<?php echo $config['base_url'];?>order">ตั้งค่าหลังบ้าน</a></li>
           <?php } ?>
         </ul>
       </nav>
       <?php
       if(!empty($_SESSION['admin'])){
        ?>
        <div id="time-login" class="text-right">
         เข้าสู่ระบบล่าสุด <a href="#"><?php echo $_SESSION['lastlogin'];?></a>
       </div>
       <?php } ?>