<?php
 include "section/header.php";
?>

        <div class="row">
            <div class="col-sm-12">

               <div class="col-sm-2" style="margin-bottom:25px;">

                  <?php
                   include "section/left-menu.php";
                  ?>


               </div>

              <div class="col-sm-10" style="margin-bottom:25px;">
              <a href="<?php echo $config['base_url'];?>order">หน้าหลัก</a> / <?php echo $so['OrderId'];?>

	            <div class="col-sm-12">
	              <h2><i class="fa fa-steam-square"></i> เลขอ้างอิง : <?php echo $so['OrderId'];?></h2>                
	            </div>

              <div class="col-sm-12" style="margin-bottom:15px;">
                <h4>รายการสั่งซื้อ</h4>
                   <?php
                    for ($i=0; $i < count($sod) ; $i++) { 
                   ?>

                    <div class="media media-game">
                      <div class="media-left">
                        <a href="<?php echo $sod[$i]['SteamLink'];?>">
                          <img class="media-object" src="<?php echo $sod[$i]['SteamImage'];?>" width="100">
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="media-heading"><a href="<?php echo $sod[$i]['SteamLink'];?>"><?php echo($sod[$i]['SteamType']=='dlc'?'[DLC]':'');?> <?php echo $sod[$i]['SteamName'];?></a></h5>
                        <h5><?php echo $sod[$i]['SteamPrice'];?> THB</h5>
                      </div>
                    </div>

                  <?php
                   }
                  ?>
                  <div class="media media-game">
                    <div class="media-left">
                      <a href="#">
                        <img class="media-object" src="<?php echo $config['base_url'];?>assets/img/steam.png" width="65">
                      </a>
                    </div>
                    <div class="media-body">
                      <p class="media-heading"><a href="#">ค่าบริการ</a></p>
                      <p><?php echo $so['OrderService'];?> THB</p>
                    </div>
                  </div> 
              </div>
              <div class="col-sm-6">
                <h4>ข้อมูลสั่งซื้อ</h4>
                <p>จำนวนเงิน : <span style="font-size:22px;"><?php echo $so['OrderTotal'];?> THB</span></p> 
                <p>อีเมล์รับ Game Gift : <?php echo $so['OrderEmail'];?></p>
                <p>วันที่สั่งซื้อ : <?php echo $so['OrderDate'];?></p>
                <p>หมดอายุ : <?php echo $so['OrderExpire'];?></p>
                <p>สถานะ : 

                                              <?php
                                                if($so['OrderStatus']==1){
                                              ?>

                                              <span class="text-danger">สั่งซื้อ</span>

                                              <?php }else if($so['OrderStatus']==2){?>

                                              <span class="text-warning">ชำระเงินแล้ว</span>

                                              <?php }else if($so['OrderStatus']==3){?>

                                              <span class="text-success">สำเร็จ</span>

                                              <?php }else{?>

                                              <span class="text-info">หมดอายุ</span>

                                              <?php } ?>
                </p>
                <p>IP Address : <?php echo $so['IP'];?></p>
              </div>

              <div class="col-sm-6">
                <h4>ข้อมูลชำระเงิน</h4>

            <?php
              if($so['OrderStatus'] > 1 && $so['OrderPay']==1){
            ?>


                        <p>วิธีชำระ : <span class="text-success">ผ่านธนาคาร</span></p>
                        <p><?php echo $sp['BankDetail'];?></p>
                        <p>ยอดเงินที่โอน : <span class="text-success" style="font-size:22px;"><?php echo $sp['PaymentTotal'];?> THB</span></p>
                        <p>ชื่อผู้โอน : <?php echo $sp['Name'];?></p>
                        <p>เบอร์โทร : <?php echo $sp['telephone'];?></p>
                        <p>อีเมล์ : <?php echo $sp['email'];?></p>
                        <p>วันที่ชำระ : <?php echo $sp['PaymentDate'];?></p>
                        <p>IP Address : <?php echo $sp['IP'];?></p>

                        <p>หลักฐาน :  
                          <a class="btn btn-primary btn-xs" role="button" data-toggle="collapse" href="#collapseImg" aria-expanded="false" aria-controls="collapseImg">
                           คลิกดูรูป
                          </a>
                        </p> 

                        <div class="collapse" id="collapseImg">

                          <img src="<?php echo $config['base_url'].$sp['PaymentImage'];?>" width="240" class="img-rounded">
  
                        </div>

                <?php }else if($so['OrderStatus'] > 1 && $so['OrderPay']==2){?>



                        <p>วิธีชำระ : <span class="text-success">ผ่านทรูมันนี่</span></p>
                        <p>จำนวนเงิน : <span class="text-success" style="font-size:22px;"><?php echo $st['TruemoneyTotal'];?> THB</span></p>
                        <p>รหัสทรูมันนี่ 14 หลัก :</p>  


                        <?php
                          for ($i=0; $i < count($std) ; $i++) { 
                           

                            echo '<p class="text-success">รหัสบัตร ['.$std[$i]['TruemoneyCode'].'] <span><i class="fa fa-check"></i> +'.$std[$i]['TruemoneyAmount'].'</span></p>';


                          }
                        ?>
                        <p>ชื่อผู้โอน : <?php echo $st['Name'];?></p>
                        <p>เบอร์โทร : <?php echo $st['telephone'];?></p>
                        <p>อีเมล์ : <?php echo $st['email'];?></p>
                        <p>วันที่ชำระ : <?php echo $st['TruemoneyDate'];?></p>
                        <p>IP Address : <?php echo $st['IP'];?></p>


                  <?php }else{ ?>

                  <p><span class="text-danger">ยังไม่ได้ชำระเงิน</span></p>

                <?php } ?>

              </div>

              <div class="col-sm-12" style="margin-top:15px;">

            <?php
              if($so['OrderStatus'] == 2 ){
            ?>


              <h4>ส่ง Code ให้ลูกค้าผ่านอีเมล์</h4>
                <form action="<?php echo $config['base_url'];?>pay/sendcode" method="POST">
                  <input type="hidden" name="sid" value="<?php echo session_id();?>">
                  <input type="hidden" name="oid" value="<?php echo $so['OrderId'];?>">
                  <input type="hidden" name="email" value="<?php echo $so['OrderEmail'];?>">

                  <div class="form-group">
                   <textarea class="form-control" name="message" required>
                     
                        เรียน ผู้ใช้บริการสั่งซื้อเกมส์ Steam<br>
                        เรื่อง แจ้งส่งโค้ดเกมส์ Steam เลขที่อ้างอิง <?php echo $so['OrderId'];?><br><br>

                        ท่านได้สั่งซื้อเกมส์<br><br>

                           <?php
                            for ($i=0; $i < count($sod) ; $i++) { 
                           ?>
<?php echo $i+1;?>. <a href="<?php echo $sod[$i]['SteamLink'];?>"><?php echo($sod[$i]['SteamType']=='dlc'?'[DLC]':'');?> <?php echo $sod[$i]['SteamName'];?></a><br>
                              ราคา <?php echo $sod[$i]['SteamPrice'];?> THB<br><br>
                          <?php
                           }
                          ?>

                              รวมราคา <?php echo $so['OrderTotal'];?> THB (รวมค่าบริการแล้ว) <?php echo ($so['OrderPay']==2? 'ชำระด้วยทรูมันนี่ '.$st['TruemoneyTotal'].' THB':''); ?> <br><br>

                        ทางเราได้ตรวจสอบข้อมูลแล้ว และได้ซื้อเกมส์ตามที่ท่านประสงค์แล้ว<br>
                        กรุณาตรวจสอบที่เว็บไซต์ STEAM.COM เข้าสู่ระบบโดยใช้อีเมล์ที่ท่านแจ้งสั่งซื้อกับทางเรา<br><br>

                        สอบถามข้อมูลเพิ่มเติมได้ที่ email@email.com โทร. 02-123-4567<br><br>

                        ขอแสดงความนับถือ<br>
                        STEAM GIFT<br>

                   </textarea>                    
                  </div>
                  <div class="form-group text-right">
                  <button type="submit" class="btn btn-success btn-sm">ส่ง Code</button>
                  </div>
                 </form>

<script src="<?php echo $config['base_url'];?>application/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
  tinymce.init({
    selector: 'textarea',
    setup: function (editor) {
        editor.on('change', function () {
            editor.save();
        });
    },
    height: 450,
    theme: 'modern',
    convert_urls: false,
    plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools jbimages'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages',
    toolbar2: 'print preview media  fontsizeselect | forecolor backcolor emoticons',
    image_advtab: true,
    templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
    '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
    '//www.tinymce.com/css/codepen.min.css'
    ]
  });
</script>

                 <?php
                   }else if($so['OrderStatus'] == 3 ){

                    echo '<div class="text-center"><h4>ส่ง Code ให้ลูกค้าเรียบร้อยแล้ว</h4></div>';

                   }
                 ?>

              </div>

            </div>
           
        </div>



    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/js/index.js"></script>
    <script type="text/javascript" src="<?php echo $config['base_url'];?>assets/lib/js/bootstrap.min.js"></script>