<?php
  $steam_slides = ORM::for_table('steam_slides')->find_many();
  if(count($steam_slides) != '0'){
?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    for ($i=0; $i < count($steam_slides); $i++) {
      if($i == '0'){
        $active = 'active';
      }else{
        $active = '';
      }
      echo '<li data-target="#carousel-example-generic" data-slide-to="0" class="'.$active.'"></li>';
    }
    ?>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php
    for ($i=0; $i < count($steam_slides); $i++) {
      if($i == '0'){
        $active = 'active';
      }else{
        $active = '';
      }
      echo '<div class="item '.$active.'">
      <a target="_BLANK" href="'.$steam_slides[$i]['link'].'"><img src="'.$steam_slides[$i]['linkimage'].'" alt="'.$steam_slides[$i]['name'].'">
        <div class="carousel-caption">
          <!--<h3>'.$steam_slides[$i]['name'].'</h3>
          <p>'.$steam_slides[$i]['detail'].'</p>-->
        </div>
      </a>
    </div>';
  }
  ?>
</div>

<!-- Controls -->
<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
</div>
<?php
 }
?>

<div class="col-sm-12">
  <h2><i class="fa fa-steam-square"></i> ฝากซื้อเกมส์ STEAM</h2>                
</div>
<form id="gameforms">
  <div class="form-group col-sm-10">
    <input type="text" class="form-control" id="urlsteam" name="urlsteam" placeholder="http://store.steampowered.com/app/00001/" required>
  </div>
  <button type="submit" class="btn btn-danger col-sm-2">ตกลง</button>
</form>
<div class="col-sm-12">
 <?php

 echo $s['detail'];

 ?>
</div>
<div id="wrapgame" class="col-sm-12 wrap-media-game">


</div>

<!-- Modal -->
<div class="modal fade" id="myModal01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">โปรดกรอกอีเมล์ STEAM ของท่านสำหรับรับ GAME GIFT</h4>
      </div>
      <div class="modal-body">
        <form action="<?php echo $config['base_url'];?>pay/bank" method="POST">
          <div class="form-group col">
            <input type="hidden" name="sid" value="<?php echo session_id();?>">
            <input type="hidden" name="paytype" value="1">
            <input type="email" class="form-control" id="emailbank" name="emailbank" placeholder="อีเมล์ STEAM ของท่านสำหรับรับ GAME GIFT" required>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
          <button type="submit" name="submit_bank" class="btn btn-success">ตกลง</button></form>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="myModal02" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">โปรดกรอกอีเมล์ STEAM ของท่านสำหรับรับ GAME GIFT</h4>
        </div>
        <div class="modal-body">
          <form action="<?php echo $config['base_url'];?>pay/bank" method="POST">
            <div class="form-group col">
              <input type="hidden" name="sid" value="<?php echo session_id();?>">
              <input type="hidden" name="paytype" value="2">
              <input type="email" class="form-control" id="emailsteam" name="emailbank" placeholder="อีเมล์ STEAM ของท่านสำหรับรับ GAME GIFT" required>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            <button type="submit" name="submit_bank" class="btn btn-success">ตกลง</button></form>
          </div>
        </div>
      </div>
    </div>
