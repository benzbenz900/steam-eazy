<?php
 include "section/header.php";
?>


        <div class="row">
            <div class="col-sm-12">

            	 <div class="col-sm-2" style="margin-bottom:25px;">

					<?php
					 include "section/left-menu.php";
					?>


            	 </div>

	            <div class="col-sm-10" style="margin-bottom:25px;">
	              <h2><i class="fa fa-steam-square"></i> แก้ไขหน้าเว็บไซต์</h2>                

					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'];?>setting/editpage">

					  <div class="form-group">
					    <div class="col-sm-12">

							<?php
								if(isset($_SESSION['process'])){
							?>

							<div class="alert alert-success alert-dismissible" role="alert">
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							  <strong>สำเร็จ!</strong> แก้ไขหน้าเว็บไซต์เรียบร้อยแล้ว
							</div>

							<?php
							   
							   unset($_SESSION['process']);

								}
							?>


					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-12"><h4>ค่าบริการ</h4></label> 
					    <div class="col-sm-12">
					      <textarea class="form-control editor" name="service"><?php echo $sp['service'];?></textarea>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-12"><h4>คำถามที่พบบ่อย</h4></label>
					    <div class="col-sm-12">
					      <textarea class="form-control editor" name="answer"><?php echo $sp['answer'];?></textarea>
					    </div>
					  </div>

					  <div class="form-group">
					    <label class="col-sm-12"><h4>ติดต่อเรา</h4></label>
					    <div class="col-sm-12">
					      <textarea class="form-control editor" name="contact"><?php echo $sp['contact'];?></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-12">
					      <button type="submit" class="btn btn-success">บันทึก</button>
					    </div>
					  </div>
					</form>

            </div>
           
        </div>





<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
<script>
  CKEDITOR.replaceAll('editor');
</script>
