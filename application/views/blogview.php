<?php
include "section/header.php";
?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-10" style="margin-bottom:25px;">
			<h2><i class="fa fa-steam-square"></i> <?php echo $b['name']?></h2>
			<?php echo $b['description']?>
		</div>
		<div class="col-md-2">
              <?php
          for ($i=0; $i < count($steam_blog); $i++) {
            $active = ($page == 'blogview' && $id == $steam_blog[$i]['id']) ? 'active' : '';
            if($steam_blog[$i]['showon'] == '2' || $steam_blog[$i]['showon'] == '0'){
            echo '<li class="'.$active.'"><a href="'.$config['base_url'].'blogview/index/'.$steam_blog[$i]['id'].'">'.$steam_blog[$i]['name'].'</a></li>';
            }
          }
          ?>
		</div>
	</div>

	<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
	<script>
		CKEDITOR.replace('description');
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>