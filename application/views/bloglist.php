<?php
include "section/header.php";
?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2" style="margin-bottom:25px;">
			<?php
			include "section/left-menu.php";
			?>
		</div>
		<div class="col-sm-10" style="margin-bottom:25px;">
			<h2><i class="fa fa-steam-square"></i> หน้าเว็บ</h2>
			<button class="btn btn-success" data-toggle="modal" data-target="#myModal">เพิ่มหน้า</button>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>ชื่อหน้า</th>
						<th>สถานะ</th>
						<th>ลบ</th>
					</tr>
				</thead>
				<tbody>
					<?php
					for ($i=0; $i < count($b); $i++) {
						$statusa = ($b[$i]['status'] == '1') ? '<span class="label label-success">แสดงผล</span>' : '<span class="label label-danger">ไม่แสดง</span>' ;
						echo '
						<tr>
							<th scope="row">'.$b[$i]['id'].'</th>
							<td>'.$b[$i]['name'].'</td>
							<td>'.$statusa.'</td>
							<td><a class="btn btn-info btn-xs" href="'.$config['base_url'].'bloglist/edit/'.$b[$i]['id'].'" role="button">แก้ไข</a> <a class="btn btn-danger btn-xs" href="'.$config['base_url'].'bloglist/del/'.$b[$i]['id'].'" role="button">ลบ</a></td>
						</tr>
						';
					}
					?>
				</tbody>
			</table>
		</div>
	</div>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" style="width: 70%;" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">เพิ่มหน้าใหม่</h4>
				</div>
				<div class="modal-body">

					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'].'bloglist/add';?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อหน้า</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" placeholder="ชื่อหน้า">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">ซ้อน/แสดง</label>
							<div class="col-sm-10">
								<select class="form-control" name="status">
									<option value="1">แสดง</option>
									<option value="0">ซ้อน</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">แสดงที่</label>
							<div class="col-sm-10">
								<select class="form-control" name="showon">
									<option value="0">ทั้งคู่</option>
									<option value="1">ด้านบน</option>
									<option value="2">ด้านข้าง</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">รายละเอียด</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="description" placeholder="รายละเอียด"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
								<button type="submit" class="btn btn-primary">เพิ่ม</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

	<!-- <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script> -->
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<!-- <script src="//cdn.ckeditor.com/4.5.11/full-all/ckeditor.js"></script> -->
	<script>
		CKEDITOR.replace('description');
	</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>