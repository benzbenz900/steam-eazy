<?php
include "section/header.php";
?>
<div class="row">
	<div class="col-sm-12">
		<div class="col-sm-2" style="margin-bottom:25px;">
			<?php
			include "section/left-menu.php";
			?>
		</div>
		<div class="col-sm-10" style="margin-bottom:25px;">
			<h2><i class="fa fa-steam-square"></i> แก้ไขสไลต์</h2>
					<form class="form-horizontal" method="post" action="<?php echo $config['base_url'].'slides/edit/'.$idedit;?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">ชื่อเสไลต์</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" placeholder="ชื่อเสไลต์" value="<?php echo $steam_slides['name'];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">รายละเอียด</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="detail" placeholder="รายละเอียด" value="<?php echo $steam_slides['detail'];?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Link รูป</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="linkimage" placeholder="Link รูป" value="<?php echo $steam_slides['linkimage'];?>">
							</div>
							<img src="<?php echo $steam_slides['linkimage'];?>" style="width: 30%;">
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Link Web</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="link" placeholder="Link Web" value="<?php echo $steam_slides['link'];?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							<input type="hidden" name="action" value="save">
								<button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
								<button type="submit" class="btn btn-primary">แก้ไข</button>
							</div>
						</div>
					</form>
		</div>
	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>