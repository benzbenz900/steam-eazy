<?php

class Upload_image {

	public function UploadSlip($images,$imagestmp,$floder){

                          $type =  array('jpg','JPG','JPEG','jpeg','png'); 

                          if($images!= "")
                           {
                             $imageType = pathinfo($images,PATHINFO_EXTENSION);

                             if(!in_array($imageType,$type) ) {

                               exit();

                             }else {


                              $slip = "slip_".time().rand(0,999).".".$imageType;

                              $slipimage=$floder.$slip;

                              $this->UploadImage($imagestmp,$slipimage,'',$imageType);

                          }
                      } 

                 return $slipimage; 

		}



  public function UploadLogo($images,$imagestmp,$floder){

                          $type =  array('jpg','JPG','JPEG','jpeg','png'); 

                          if($images!= "")
                           {
                             $imageType = pathinfo($images,PATHINFO_EXTENSION);

                             if(!in_array($imageType,$type) ) {

                               exit();

                             }else {


                              $slip = "logo_".time().rand(0,999).".".$imageType;

                              $slipimage=$floder.$slip;

                              $this->UploadImage($imagestmp,$slipimage,'',$imageType);

                          }
                      } 

                 return $slipimage; 

    }

  public function UploadBg($images,$imagestmp,$floder){

                          $type =  array('jpg','JPG','JPEG','jpeg','png'); 

                          if($images!= "")
                           {
                             $imageType = pathinfo($images,PATHINFO_EXTENSION);

                             if(!in_array($imageType,$type) ) {

                               exit();

                             }else {


                              $slip = "bg_".time().rand(0,999).".".$imageType;

                              $slipimage=$floder.$slip;

                              $this->UploadImage($imagestmp,$slipimage,'',$imageType);

                          }
                      } 

                 return $slipimage; 

    }


      public function UploadImage($files,$path,$rotate,$imageType) {

      if(!empty($files)){

                  if ($rotate=='1' || $rotate=='undefined' || empty($rotate)) {

                      move_uploaded_file($files,$path);     
                           
                  }else if (!empty($rotate)) {

                             $imageType = strtolower($imageType);

                              if($imageType =="png"){ 

                                $files = imagecreatefrompng($files);

                              } else { 

                                $files = imagecreatefromjpeg($files);

                              }

                      switch ($rotate) {
                        case 3:

                          $rotate2 = imagerotate($files, 180, 0);
                          break;

                        case 6:

                          $rotate2 = imagerotate($files, -90, 0);
                          break;

                        case 8:
                        
                         $rotate2 = imagerotate($files, 90, 0);
                          break;
                      }


                      if($imageType =="png"){ 

                          imagepng($rotate2,$path);

                      } else { 

                          imagejpeg($rotate2,$path,100);
                          
                      }

                    
                  }


              }
              

      }


        public function CropImage($files,$thumb,$rotate,$imageType,$height='320') {

          if(!empty($files)){

          $Size=getimagesize($files);

          $Height=$height;

          $SizeX=$Size[0];

          $SizeY=$Size[1];

          $Weight=ceil($SizeX*$Height)/$SizeY;


          $imageType = strtolower($imageType);

          if($imageType =="png"){ 

          $Image_Ori = imagecreatefrompng($files);

          } else { 

          $Image_Ori = imagecreatefromjpeg($files);

          }

          $Image_Fin=imagecreatetruecolor($Weight,$Height);

          $ImageX=imagesx($Image_Ori);

          $ImageY=imagesy($Image_Ori);

          imagecopyresampled($Image_Fin,$Image_Ori,0,0,0,0,$Weight,$Height,$ImageX,$ImageY);


                if (!empty($rotate)) {

                  switch ($rotate) {
                    case 3:
                      $Image_Fin = imagerotate($Image_Fin, 180, 0);
                      break;
                    case 6:
                      $Image_Fin = imagerotate($Image_Fin, -90, 0);
                      break;
                    case 8:
                      $Image_Fin = imagerotate($Image_Fin, 90, 0);
                      break;
                  }
                
              }
          
            if($imageType =="png"){ 

                imagepng($Image_Fin,$thumb);

            } else { 

                imagejpeg($Image_Fin,$thumb,100);
                
            }

        }

    }

        public function CropThumb($files,$thumb,$rotate,$imageType,$width='250',$height='250') {

          if(!empty($files)){

          
          list($w_ori,$h_ori) = getimagesize($files);

                $w_new=$width;
                $h_new=$height;
                $retio_w=$w_new/$w_ori;
                $retio_h=$h_new/$h_ori;

                if($w_ori > $h_ori){

                $w_crop=$w_ori * $retio_h; 
                $h_crop=$h_new;

                }else if($w_ori < $h_ori){

                $w_crop=$w_new; 
                $h_crop=$h_ori * $retio_w;

                }else {

                $w_crop=$w_new; 
                $h_crop=$h_new;          

                }

          $imageType = strtolower($imageType);

          if($imageType =="png"){ 

          $image_1 = imagecreatefrompng($files);

          } else { 

          $image_1 = imagecreatefromjpeg($files);

          }

          $image_2=imagecreatetruecolor($w_new, $h_new);

          imagecopyresampled($image_2, $image_1,-(($w_crop/2) - ($w_new/2)),-(($h_crop/2) - ($h_new/2)), 0, 0, $w_crop, $h_crop, $w_ori, $h_ori);


                if (!empty($rotate)) {

                  switch ($rotate) {
                    case 3:
                      $image_2 = imagerotate($image_2, 180, 0);
                      break;
                    case 6:
                      $image_2 = imagerotate($image_2, -90, 0);
                      break;
                    case 8:
                      $image_2 = imagerotate($image_2, 90, 0);
                      break;
                  }
                
              }
          

            if($imageType =="png"){ 

                imagepng($image_2,$thumb);

            } else { 

                imagejpeg($image_2,$thumb,100);
                
            }


        }

        }





}

?>

