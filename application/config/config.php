<?php

$config['base_url'] = 'http://demo.mul.pw/steameazy/'; // Base URL including trailing slash (e.g. http://localhost/)

$config['default_controller'] = 'main'; // Default controller to load
$config['error_controller'] = 'error'; // Controller used for errors (e.g. 404, 500 etc)

$db_host = 'localhost'; // Database host (e.g. localhost)
$db_name = 'admin_steam'; // Database name
$db_user= 'admin_steam'; // Database username
$db_password = '0YC5GIyw'; // Database password

 // Connect to the demo database file
ORM::configure("mysql:host=$db_host;dbname=$db_name");
ORM::configure('username', $db_user);
ORM::configure('password', $db_password);
ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
ORM::configure('return_result_sets', true);
ORM::configure('logging', true);

?>