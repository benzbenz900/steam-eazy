<?php

class setting extends Controller {
	
	function index()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{

		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
		$template = $this->loadView('setting');
		$template->set('s',$steam_setting);
		$template->set('title',"ตั้งค่าเว็บ");
		$template->set('page',"setting");
		$template->render();

	   }

	}
	
	function page()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{

		$steam_page = ORM::for_table('steam_page')->where('id','1')->find_one();
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
		$template = $this->loadView('page');
		$template->set('sp',$steam_page);
		$template->set('s',$steam_setting);
		$template->set('title',"แก้ไขหน้าเว็บ");
		$template->set('page',"page");
		$template->render();

	   }

	}

	function payment()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{

		$steam_bank= ORM::for_table('steam_bank')->find_many();
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
		$template = $this->loadView('payment');
		$template->set('sp',$steam_bank);
		$template->set('s',$steam_setting);
		$template->set('title',"แก้ไขบัญชีธนาคาร");
		$template->set('page',"bank");
		$template->render();

	   }

	}





    	function edit()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{

		$setting = ORM::for_table('steam_setting')->find_one('1');

        $upload=$this->loadHelper('upload_image');

        if(!empty($_FILES['logo']["name"])){

        $image = $upload->UploadLogo($_FILES['logo']["name"],$_FILES['logo']["tmp_name"],'assets/img/logo/');

        $setting->logoweb =$image;

        }


        if(!empty($_FILES['bg']["name"])){

        $image2 = $upload->UploadBg($_FILES['bg']["name"],$_FILES['bg']["tmp_name"],'assets/img/bg/');

        $setting->bgimage =$image2;

        }


		$setting->nameweb =$_POST['nameweb'];
		$setting->openweb =$_POST['openweb'];
		$setting->detail =$_POST['detail'];
		$setting->footer =$_POST['footer'];
		$setting->email =$_POST['email'];
		$setting->tmuid =$_POST['tmuid'];
		$setting->tmkey =$_POST['tmkey'];
		$setting->FBappid =$_POST['FBappid'];
		$setting->phone =$_POST['phone'];
		$setting->username =$_POST['username'];
		$setting->password =$_POST['password'];
		
        $setting->save();

        $_SESSION['process']='success';

        $this->redirect('setting');


	  }

	}

    	function editpage()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{

		$setting = ORM::for_table('steam_page')->find_one('1');
		$setting->service =$_POST['service'];
		$setting->answer =$_POST['answer'];
		$setting->contact =$_POST['contact'];
		
        $setting->save();

        $_SESSION['process']='success';

        $this->redirect('setting/page');


	  }

	}

    	function editpayment()
	{

		if(!isset($_SESSION['admin'])){

			$this->redirect('login');

		}else{


		$setting = ORM::for_table('steam_bank')->find_one($_POST['id']);
		$setting->bankname =$_POST['bank'];
		$setting->name =$_POST['name'];
		$setting->no =$_POST['no'];
		$setting->brance =$_POST['branch'];
		$setting->type =$_POST['type'];
		$setting->status =$_POST['open'];

        $setting->save();

        $_SESSION['process']='success';

        $this->redirect('setting/payment');


	  }

	}
}

?>
