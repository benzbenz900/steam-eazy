<?php

class Order extends Controller {
	
	function index()
	{
		if(!isset($_SESSION['admin'])){

			$this->redirect('error404');

		}else{

		$so = ORM::for_table('steam_order')->where_raw('(`OrderStatus` != ?)', array('0'))->find_many();
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

		$template = $this->loadView('admin_order');
		$template->set('s',$steam_setting);
		$template->set('title',"จัดการสั่งซื้อ");
		$template->set('page',"order");
		$template->set('so',$so);
		$template->render();


	  }
	}
	
	function detail($oid="")
	{
		if(!isset($_SESSION['admin'])){

			$this->redirect('error404');

		}else{

		$so = ORM::for_table('steam_order')->where('OrderId',$oid)->where_raw('(`OrderStatus` != ?)', array('0'))->find_one();
		$sod = ORM::for_table('steam_order_detail')->where('OrderId',$oid)->where_raw('(`Status` != ?)', array('0'))->find_many();

		$template = $this->loadView('admin_order_detail');

		 if($so['OrderStatus'] > 1 && $so['OrderPay']==1){

		  $sp = ORM::for_table('steam_payment')->where('OrderId',$oid)->find_one();

		  $template->set('sp',$sp);

	     }else{

		  $st = ORM::for_table('steam_truemoney')->where('OrderId',$oid)->find_one();
		  $std = ORM::for_table('steam_truemoney_code')->where('OrderId',$oid)->find_many();

		  $template->set('st',$st);
		  $template->set('std',$std);

	     }
	     $steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

		$template->set('title',"รายละเอีดสั่งซื้อ oid : ".$oid);
		$template->set('page',"orderdetail");
		$template->set('s',$steam_setting);
		$template->set('so',$so);
		$template->set('sod',$sod);
		$template->render();


	  }
	}
    
}

?>
