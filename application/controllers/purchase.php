<?php

class Purchase extends Controller {
	
	function index()
	{
		if(!isset($_SESSION['oid'])){

			$this->redirect('');

		}else{


		$so = ORM::for_table('steam_order')->where('OrderId',$_SESSION['oid'])->find_one();	

		$sod = ORM::for_table('steam_order_detail')->where('OrderId',$_SESSION['oid'])->order_by_asc('id')->find_many();

		$steam_bank= ORM::for_table('steam_bank')->where('status','1')->order_by_asc('id')->find_many();

		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

		$template = $this->loadView('purchase');
		$template->set('sod',$sod);	
		$template->set('so',$so);	
		$template->set('sp',$steam_bank);
		$template->set('s',$steam_setting);
		$template->set('title',"ชำระค่าบริการผ่านธนาคาร");
		$template->set('page',"purchase");
		$template->render();

	  }

	}

    
}

?>
