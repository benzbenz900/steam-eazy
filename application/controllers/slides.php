<?php

class slides extends Controller {
	
	function index()
	{
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
		$steam_slides = ORM::for_table('steam_slides')->find_many();
		
		
		$template = $this->loadView('settingslides');
		$template->set('steam_slides',$steam_slides);
		$template->set('s',$steam_setting);
		$template->set('title',"เพิ่มสไลต์");
		$template->render();

	}

	function del($id)
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_slides = ORM::for_table('steam_slides')->find_one($id);
			$steam_slides->delete();
			$this->redirect('slides');
		}
	}

	public function add()
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_slides = ORM::for_table('steam_slides')->create();
			$steam_slides->set('name', $_POST['name']);
			$steam_slides->set('detail', $_POST['detail']);
			$steam_slides->set('linkimage', $_POST['linkimage']);
			$steam_slides->set('link', $_POST['link']);
			$steam_slides->save();
			$this->redirect('slides');
		}
	}

	public function edit($id)
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_slides = ORM::for_table('steam_slides')->find_one($id);
			if(isset($_POST['action']) && $_POST['action'] == 'save'){
				$steam_slides->set('name', $_POST['name']);
				$steam_slides->set('detail', $_POST['detail']);
				$steam_slides->set('linkimage', $_POST['linkimage']);
				$steam_slides->set('link', $_POST['link']);
				$steam_slides->save();
			}
			$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

			$template = $this->loadView('settingslidesedit');
			$template->set('steam_slides',$steam_slides);
			$template->set('s',$steam_setting);
			$template->set('title',"แก้ไขสไลต์");
			$template->set('idedit',$id);
			$template->render();
		}
	}


}

?>
