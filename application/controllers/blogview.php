<?php
class blogview extends Controller {
	function index($id)
	{
			$steam_blog = ORM::for_table('steam_blog')->find_one($id);
			$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
			$template = $this->loadView('blogview');
			$template->set('b',$steam_blog);
			$template->set('s',$steam_setting);
			$template->set('title',$steam_blog['name']);
			$template->set('id',$steam_blog['id']);
			$template->set('page',"blogview");
			$template->render();
	}

}

?>
