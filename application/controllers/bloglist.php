<?php
class bloglist extends Controller {
	function index()
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_blog = ORM::for_table('steam_blog')->find_many();
			$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
			$template = $this->loadView('bloglist');
			$template->set('b',$steam_blog);
			$template->set('s',$steam_setting);
			$template->set('title',"เพิ่มหน้าเว็บ");
			$template->set('page',"bloglist");
			$template->render();
		}
	}

	function del($id)
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_blog = ORM::for_table('steam_blog')->find_one($id);
			$steam_blog->delete();
			$this->redirect('bloglist');
		}
	}

	function add()
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_blog = ORM::for_table('steam_blog')->create();
			$steam_blog->set('name', $_POST['name']);
			$steam_blog->set('description', $_POST['description']);
			$steam_blog->set('status', $_POST['status']);
			$steam_blog->set('showon', $_POST['showon']);
			$steam_blog->save();
			$this->redirect('bloglist');
		}
	}

	function edit($id)
	{
		if(!isset($_SESSION['admin'])){
			$this->redirect('login');
		}else{
			$steam_blog = ORM::for_table('steam_blog')->find_one($id);
			$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
			$template = $this->loadView('blogedit');
			if(isset($_POST['action']) && $_POST['action'] == 'save'){
				$steam_blog->set('name', $_POST['name']);
				$steam_blog->set('description', $_POST['description']);
				$steam_blog->set('status', $_POST['status']);
				$steam_blog->set('showon', $_POST['showon']);
				$steam_blog->save();
			}
			$template->set('title',"แก้ไขหน้าเว็บ");
			$template->set('b',$steam_blog);
			$template->set('page',"bloglist");
			$template->set('s',$steam_setting);
			$template->set('idedit',$id);
			$template->render();
		}
	}

}

?>
