<?php

class Contact extends Controller {
	
	function index()
	{
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
		$steam_page = ORM::for_table('steam_page')->where('id','1')->find_one();
		
		$template = $this->loadView('index');
		$template->set('body',$steam_page['contact']);
		$template->set('s',$steam_setting);
		$template->set('title',"ติดต่อเรา");
		$template->set('page',"contact");
		$template->render();

	}

    
}

?>
