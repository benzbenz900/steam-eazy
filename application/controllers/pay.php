<?php

class Pay extends Controller {
	
	function bank()
	{

      if(!empty($_POST['sid']) && !empty($_POST['emailbank'])){

        $paytype=$_POST['paytype'];
        $email=$_POST['emailbank'];

        $d=date("Y-m-d H:i:s"); 

        $exd=date("Y-m-d H:i:s", mktime(date("H")+1, date("i")+0, date("s")+0, date("m")+0 , date("d")+0, date("Y")+0));


        $lastid = ORM::for_table('steam_order')->order_by_desc('id')->find_one();

        $id =$lastid['id']+1;

        $oid= 'ST'.substr('00000'.$id,-5);


        foreach ($_SESSION['gid'] as $v) {

            $id=$_SESSION['gid'][$v];
            $name=$_SESSION['name'][$v];
            $type=$_SESSION['type'][$v];
            $img=$_SESSION['img'][$v];
            $price=$_SESSION['price'][$v];
            $sprice=$_SESSION['sprice'][$v];
            $discount=$_SESSION['discount'][$v];

            if($type=='package'){

                $url='http://store.steampowered.com/sub/'.$id;

            }else{

                $url='http://store.steampowered.com/app/'.$id;

            }


            $detail = ORM::for_table('steam_order_detail')->create(); 
            $detail->OrderId =$oid;
            $detail->SteamId =$id;
            $detail->SteamName =$name;
            $detail->SteamImage =$img;
            $detail->SteamType =$type;
            $detail->SteamPrice =$price;
            $detail->SteamSprice =$sprice;
            $detail->SteamDiscount =$discount;
            $detail->SteamLink =$url;
            $detail->Status ='1';
            $detail->save();


        }


         if($paytype==1){

         $service=$_SESSION['total_service'];
         $bank= $_SESSION['total_bank'].'.'.rand(11,99);


         }else{

         $service='14.9%';
         $bank= $_SESSION['total_true'];

         }


        $order = ORM::for_table('steam_order')->create();

        $order->OrderId =$oid;
        $order->OrderEmail =$email;
        $order->OrderTotal =$bank;
        $order->OrderService =$service;
        $order->OrderPay =$paytype;
        $order->OrderDate =$d;
        $order->OrderExpire =$exd;
        $order->OrderStatus ='1';
        $order->IP =$_SERVER['REMOTE_ADDR'];
        $order->save();

        unset($_SESSION['gid']);
        unset($_SESSION['name']);
        unset($_SESSION['type']);
        unset($_SESSION['img']);
        unset($_SESSION['price']);
        unset($_SESSION['sprice']);
        unset($_SESSION['discount']);
        unset($_SESSION['total_service']);
        unset($_SESSION['total_bank']);
        unset($_SESSION['total_true']);

        $_SESSION['oid']=$oid;



         if($paytype==1){

         $this->redirect('purchase'); 

         }else{

         $this->redirect('truemoney'); 

         }


      }else{

        $this->redirect('error404'); 


      }


  }


 function confirm()
  {

     if(!empty($_POST['sid']) && !empty($_POST['total'])){

      $bank=$_POST['bank'];
      $name=$_POST['name'];
      $telephone=$_POST['telephone'];
      $email=$_POST['email'];
      $total=$_POST['total'];
      $date=$_POST['date'];
      $oid=$_POST['oid'];

      $d=date("Y-m-d H:i:s");

        $payment = ORM::for_table('steam_payment')->create();

        if(!empty($_FILES['file']["name"])){

        $upload=$this->loadHelper('upload_image');

        $image = $upload->UploadSlip($_FILES['file']["name"],$_FILES['file']["tmp_name"],'assets/img/slip/');

        $payment->PaymentImage =$image;

        }
        
        $steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

        $payment->OrderId =$oid;
        $payment->BankDetail =$bank;
        $payment->Name =$name;
        $payment->telephone =$telephone;
        $payment->email =$email;
        $payment->PaymentTotal =$total;
        $payment->PaymentDate =$date;
        $payment->PaymentStatus ='1';
        $payment->IP =$_SERVER['REMOTE_ADDR'];
        $payment->save();


        $order = ORM::for_table('steam_order')->where('OrderId',$oid)->find_one();
        $order->OrderStatus ='2';
        $order->save();


        $action=$this->loadModel('action');

        $messages ='แจ้งโอนเงินผ่านธนาคาร<br>เลขที่อ้างอิง '.$oid.' <br>
        <br> '.$bank.' <br> ยอดเงิน : '.$total.' <br> ผู้โอน : '.$name.' <br> วัน-เวลาโอน : '.$date.' <br> โทรศัพท์ : '.$telephone.' <br> อีเมล์ : '.$email
        .'<br><br><br> โปรดตรวจสอบข้อมูลเพื่อสั่งซื้อเกมส์ตามการสั่งซื้อก่อนทุกครั้ง';

        $sendemail = $action->sendemail($name,'แจ้งโอนเงินผ่านธนาคาร',$messages,$steam_setting['email'],$email);


        $_SESSION['oid']=$oid;

        $this->redirect('purchase'); 

     }else{

        $this->redirect('error404'); 

     }

  }

 function confirmtrue()
  {

     if(!empty($_POST['sid']) && !empty($_POST['oid'])){


      $name=$_POST['name'];
      $telephone=$_POST['telephone'];
      $email=$_POST['email'];
      $date=$_POST['date'];
      $oid=$_POST['oid'];
      $total=$_POST['total'];

      $d=date("Y-m-d H:i:s");



        $payment = ORM::for_table('steam_truemoney')->create();
        $payment->OrderId =$oid;
        $payment->Name =$name;
        $payment->telephone =$telephone;
        $payment->email =$email;
        $payment->TruemoneyTotal =$total;
        $payment->TruemoneyDate =$d;
        $payment->TruemoneyStatus ='1';
        $payment->IP =$_SERVER['REMOTE_ADDR'];
        $payment->save();



        $order = ORM::for_table('steam_order')->where('OrderId',$oid)->find_one();
        $order->OrderStatus ='2';
        $order->save();


        $action=$this->loadModel('action');

        $messages ='แจ้งโอนเงินทูมันนี่<br>เลขที่อ้างอิง '.$oid.' <br>
        <br> '.$bank.' <br> ยอดเงิน : '.$total.' <br> ผู้โอน : '.$name.' <br> วัน-เวลาชำระ : '.$date.' <br> โทรศัพท์ : '.$telephone.' <br> อีเมล์ : '.$email
        .'<br><br><br> โปรดตรวจสอบข้อมูลเพื่อสั่งซื้อเกมส์ตามการสั่งซื้อก่อนทุกครั้ง';

        $steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
        $sendemail = $action->sendemail($name,'แจ้งโอนเงินผ่านทูมันนี่',$messages,$steam_setting['email'],$email);


        $_SESSION['oid']=$oid;

        $this->redirect('truemoney'); 

     }else{

        $this->redirect('error404'); 

     }

  }



 function truepay()
  {

	require_once('application/plugins/common/AES.php');
  $steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();
	define('API_PASSKEY', $steam_setting['tmkey']);

	if(isset($_GET['request']))
	{

		$aes = new Crypt_AES();
		$aes->setKey(API_PASSKEY);
		$_GET['request'] = base64_decode(strtr($_GET['request'], '-_,', '+/='));
		$_GET['request'] = $aes->decrypt($_GET['request']);
		
		if($_GET['request'] != false)
		{
			parse_str($_GET['request'],$request);
			$email = base64_decode($request['Ref1']);
			$oid = base64_decode($request['Ref2']);
			$service = base64_decode($request['Ref3']);
			$txid = $request['TXID'];
			$cardcard_password = $request['cashcard_password'];
			$cardcard_amount = $request['cashcard_amount'];
			$client_ip = $request['client_ip'];

		$d=date("Y-m-d H:i:s");

        $trueid = ORM::for_table('steam_truemoney_code')->create();
        $trueid->OrderId =$oid;
        $trueid->TruemoneyCode =$cardcard_password;
        $trueid->TruemoneyAmount =$cardcard_amount;
        $trueid->Ref1 =$email;
        $trueid->Ref2 =$oid;
        $trueid->Ref3 =$oid;
        $trueid->TruemoneyDate =$d;
        $trueid->Status ='1';
        $trueid->IP =$client_ip;
        $trueid->save();
	
			echo "SUCCEED";
		}
		else
		{

			echo "ERROR|INCORRECT_PIN";

		}

	}
	else
	{
		$this->redirect('error404'); 
	}


  }

 function checktrue()
  {
     if(!empty($_POST['oid'])){

       $st = ORM::for_table('steam_truemoney_code')->where('OrderId',$_POST['oid'])->order_by_asc('id')->find_many();


       for ($i=0; $i < count($st) ; $i++) { 
       	
       	echo '<li class="text-success"> รหัสบัตร['.$st[$i]['TruemoneyCode'].'] <span><i class="fa fa-check"></i> +'.$st[$i]['TruemoneyAmount'].'</span></li>';

       }

    }else{

		$this->redirect('error404'); 

	}

  }


 function sumtrue()
  {
     if(!empty($_POST['oid'])){

       $st = ORM::for_table('steam_truemoney_code')->where('OrderId',$_POST['oid'])->order_by_asc('id')->find_many();


       for ($i=0; $i < count($st) ; $i++) { 
       	
       	$sum +=$st[$i]['TruemoneyAmount'];

       }

       if(!empty($sum)){

       	echo $sum;

       }else{

       	echo "0";

       }


    }else{

		$this->redirect('error404'); 

	}

  }



 function reload()
  {

    if(!empty($_SESSION['oid'])){


    	$this->redirect('truemoney'); 


    }else{


    	$this->redirect('error404'); 

    }

  }

 function sendcode()
  {

     if(!empty($_POST['sid']) && !empty($_POST['email'])){

        $email=$_POST['email'];
        $messages=$_POST['message'];
        $oid=$_POST['oid'];

        $action=$this->loadModel('action');

        $sendemail = $action->sendemail('STEAM GIFT','ส่ง Game Gift',$messages,$email,'');


        $order = ORM::for_table('steam_order')->where('OrderId',$oid)->find_one();
        $order->OrderStatus ='3';
        $order->save();

        $this->redirect('order/detail/'.$oid); 

    }else{

        $this->redirect('error404'); 

     }


  }


 function cancel()
  {

     if(!empty($_POST['sid']) && !empty($_POST['oid'])){


        $oid=$_POST['oid'];

        $order = ORM::for_table('steam_order')->where('OrderId',$oid)->find_one();
        $order->OrderStatus ='0';
        $order->save();

        unset($_SESSION['oid']);

        $this->redirect(''); 

    }else{

        $this->redirect('error404'); 

     }


  }



}

?>
