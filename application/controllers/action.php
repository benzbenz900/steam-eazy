<?php

class Action extends Controller {

	public function sendmailcontact()
	{
		$steam_setting = ORM::for_table('steam_setting')->where('id','1')->find_one();

		$messages = nl2br($_POST['textdesc']).'<br>เบอร์โทร:'.$_POST['phone'].'<br>อีเมล:'.$_POST['email'];

		$to = $steam_setting['email'];
		// $to = "benzbenz900@gmail.com";
		$subject = "ติดต่อจากเว็บฝากชื้อเกมส์ ".$_POST['nameorcompany'];
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: <'.$_POST['email'].'>' . "\r\n";

		mail($to,$subject,$messages,$headers);


		$this->redirect('contact');
	}
	
	function addgame()
	{

		$url=$_POST['urlsteam'];
		if(strpos($url, 'sub') !== false){
			$appid=explode("sub/", $url);
			$subappa = true;
		}else{
			$appid=explode("app/", $url);
			$subappa = false;
		}

		$appid=trim(str_replace("/", " " , $appid[1]));


		if($subappa==false){

			if(is_numeric($appid)){
				$json = file_get_contents('http://store.steampowered.com/api/appdetails?appids='.$appid.'&cc=th');
				$ggg = json_decode($json,true);

				$free=$ggg[$appid]['data']["is_free"];
				$prices=$ggg[$appid]['data']["price_overview"]["final"];

				if($free==false && !empty($prices)){


					$name=$ggg[$appid]['data']['name'];
					$img=$ggg[$appid]['data']["header_image"];
					$type=$ggg[$appid]['data']["type"];

					$sprice0=substr($ggg[$appid]['data']["price_overview"]["initial"],0,-2);
					$sprice1=substr($ggg[$appid]['data']["price_overview"]["initial"],-2);
					$sprice=$sprice0.'.'.$sprice1;

					$price0=substr($ggg[$appid]['data']["price_overview"]["final"],0,-2);
					$price1=substr($ggg[$appid]['data']["price_overview"]["final"],-2);
					$price=$price0.'.'.$price1;

					$discount=$ggg[$appid]['data']["price_overview"]["discount_percent"];

					$dlc = $ggg[$appid]['data']["dlc"];

					$package = $ggg[$appid]['data']["package_groups"][0]["subs"];



					?>

					<div class="media media-game">
						<div class="media-left">
							<a href="<?php echo $url;?>" target="_blank">
								<img class="media-object" src="<?php echo $img;?>" width="150">
							</a>
						</div>
						<div class="media-body">
							<h5 class="media-heading"><a href="<?php echo $url;?>" target="_blank"><?php echo($type=='dlc'?'[DLC]':'');?> <?php echo $name;?></a></h5>
							<h5>ประเภท : <?php echo $type;?></h5>

							<?php
							if($discount==0){

								echo '<h5>'.$price.' THB</h5>';

							}else{

								echo '<h5><span class="sprice">'.$sprice.' THB </span> '.$price.' THB <span class="discount"> -'.$discount.'% </span></h5>';

							}
							?>

						</div>

						<?php

						if(!empty($_SESSION['gid'][$appid])){

							?>

							<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','<?php echo $type;?>')" disabled><i class="fa fa-check" aria-hidden="true"></i> เลือกแล้ว</button>

							<?php
						}else{
							?>

							<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','<?php echo $type;?>')"> หยิบลงตะกร้า </button>

							<?php
						}
						?>

					</div>

					<?php

					if(!empty($package)){


						for ($i=1; $i < count($package) ; $i++) { 

							$name = $ggg[$appid]['data']["package_groups"][0]["subs"][$i]['option_text'];
							$name =explode('-', $name);

							$packageid = $ggg[$appid]['data']["package_groups"][0]["subs"][$i]["packageid"];

							$price0=substr($ggg[$appid]['data']["package_groups"][0]["subs"][$i]['price_in_cents_with_discount'],0,-2);
							$price1=substr($ggg[$appid]['data']["package_groups"][0]["subs"][$i]['price_in_cents_with_discount'],-2);
							$price=$price0.'.'.$price1;

							$discount = $ggg[$appid]['data']["package_groups"][0]["subs"][$i]['percent_savings'];

							?>

							<div class="media media-game">
								<div class="media-left">
									<a href="http://store.steampowered.com/sub/<?php echo $packageid;?>" target="_blank">
										<img class="media-object" src="http://cdn.akamai.steamstatic.com/steam/subs/<?php echo $packageid;?>/header_ratio.jpg" width="150">
									</a>
								</div>
								<div class="media-body">
									<h5 class="media-heading"><a href="http://store.steampowered.com/sub/<?php echo $packageid;?>/" target="_blank"><?php echo $name[0];?></a></h5>
									<h5>ประเภท : PACKAGE</h5>

									<?php
									if($discount==0){

										echo '<h5>'.$price.' THB</h5>';

									}else{

										echo '<h5>'.$price.' THB <span class="discount"> ประหยัด -'.$discount.'% </span></h5>';

									}
									?>

								</div>


								<?php

								if(!empty($_SESSION['gid'][$packageid])){

									?>

									<button id="btn_<?php echo $packageid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $packageid;?>','package')" disabled><i class="fa fa-check" aria-hidden="true"></i> เลือกแล้ว</button>

									<?php
								}else{
									?>

									<button id="btn_<?php echo $packageid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $packageid;?>','package')"> หยิบลงตะกร้า </button>

									<?php
								}
								?>

							</div>

							<?php

						}

					}



					if(!empty($dlc)){


						for ($k=0; $k < count($dlc) ; $k++) { 

							$appid=$dlc[$k];

							$json = file_get_contents('http://store.steampowered.com/api/appdetails?appids='.$appid.'&cc=th');
							$ggg = json_decode($json,true);

							$free=$ggg[$appid]['data']["is_free"];

							$prices=$ggg[$appid]['data']["price_overview"]["final"];

							if($free==false && !empty($prices)){

								$name=$ggg[$appid]['data']['name'];
								$img=$ggg[$appid]['data']["header_image"];
								$type=$ggg[$appid]['data']["type"];

								$sprice0=substr($ggg[$appid]['data']["price_overview"]["initial"],0,-2);
								$sprice1=substr($ggg[$appid]['data']["price_overview"]["initial"],-2);
								$sprice=$sprice0.'.'.$sprice1;

								$price0=substr($ggg[$appid]['data']["price_overview"]["final"],0,-2);
								$price1=substr($ggg[$appid]['data']["price_overview"]["final"],-2);
								$price=$price0.'.'.$price1;

								$discount=$ggg[$appid]['data']["price_overview"]["discount_percent"];


								?>


								<div class="media media-game">
									<div class="media-left">
										<a href="http://store.steampowered.com/app/<?php echo $appid;?>/" target="_blank">
											<img class="media-object" src="<?php echo $img;?>" width="150">
										</a>
									</div>
									<div class="media-body">
										<h5 class="media-heading"><a href="http://store.steampowered.com/app/<?php echo $appid;?>/" target="_blank"><?php echo($type=='dlc'?'[DLC]':'');?> <?php echo $name;?></a></h5>
										<h5>ประเภท : <?php echo $type;?></h5>

										<?php
										if($discount==0){

											echo '<h5>'.$price.' THB</h5>';

										}else{

											echo '<h5><span class="sprice">'.$sprice.' THB </span> '.$price.' THB <span class="discount"> -'.$discount.'% </span></h5>';

										}
										?>

									</div>


									<?php

									if(!empty($_SESSION['gid'][$appid])){

										?>

										<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','<?php echo $type;?>')" disabled><i class="fa fa-check" aria-hidden="true"></i> เลือกแล้ว</button>

										<?php
									}else{
										?>

										<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','<?php echo $type;?>')"> หยิบลงตะกร้า </button>

										<?php
									}
									?>

								</div>

								<?php
							}

						}

					}

				}else{


					echo "fail";

				}

			}else{

				echo "fail";

			}
		}else{
			$json = file_get_contents('http://store.steampowered.com/api/packagedetails?packageids='.$appid.'&cc=th');
			$ggg = json_decode($json,true);
			?>
			<div class="media media-game">
				<div class="media-left">
					<a href="http://store.steampowered.com/sub/<?php echo $appid;?>" target="_blank">
						<img class="media-object" src="<?php echo $ggg[$appid]['data']['header_image'];?>" width="150">
					</a>
				</div>
				<div class="media-body">
					<h5 class="media-heading"><a href="http://store.steampowered.com/sub/<?php echo $appid;?>/" target="_blank"><?php echo $ggg[$appid]['data']['name'];?></a></h5>
					<h5>ประเภท : PACKAGE</h5>

					<?php
					$price0=substr($ggg[$appid]['data']['price']['final'],0,-2);
					$price1=substr($ggg[$appid]['data']['price']['final'],-2);
					$price=$price0.'.'.$price1;
					echo '<h5>'.$price.' THB</h5>';
					?>

				</div>


				<?php

				if(!empty($_SESSION['gid'][$appid])){

					?>

					<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','package')" disabled><i class="fa fa-check" aria-hidden="true"></i> เลือกแล้ว</button>

					<?php
				}else{
					?>

					<button id="btn_<?php echo $appid;?>" class="btn btn-success btn-sm media-game-btn" onclick="addcart('<?php echo $appid;?>','package')"> หยิบลงตะกร้า </button>

					<?php
				}
				?>

			</div>
			<?php
		}


	}


	function json($id='')
	{

		$json = file_get_contents('http://store.steampowered.com/api/appdetails?appids='.$id.'&cc=th');

                // $json = file_get_contents('http://store.steampowered.com/api/packagedetails?packageids='.$id);

		$ggg = json_decode($json,true);
		// var_dump(json_decode($json,true));

		echo '<pre>';

		var_dump($ggg);

		echo '</pre>';

	}




	function addcart()
	{

		$appid=$_POST['appid'];
		$type=$_POST['type'];


		if($type=='package'){

			$json = $json = file_get_contents('http://store.steampowered.com/api/packagedetails?packageids='.$appid.'&cc=th');
			$ggg = json_decode($json,true);

			$free=false;
			$prices=$ggg[$appid]['data']["price"]["final"];


		}else{


			$json = file_get_contents('http://store.steampowered.com/api/appdetails?appids='.$appid.'&cc=th');
			$ggg = json_decode($json,true);

			$free=$ggg[$appid]['data']["is_free"];
			$prices=$ggg[$appid]['data']["price_overview"]["final"];


		}


		if($free==false && !empty($prices)){


			if($type=='package'){

				$name=$ggg[$appid]['data']['name'];
				$img=$ggg[$appid]['data']["header_image"];

				$sprice0=substr($ggg[$appid]['data']["price"]["initial"],0,-2);
				$sprice1=substr($ggg[$appid]['data']["price"]["initial"],-2);
				$sprice=$sprice0.'.'.$sprice1;

				$price0=substr($ggg[$appid]['data']["price"]["final"],0,-2);
				$price1=substr($ggg[$appid]['data']["price"]["final"],-2);
				$price=$price0.'.'.$price1;

				$discount=$ggg[$appid]['data']["price"]["discount_percent"];

			}else{

				$name=$ggg[$appid]['data']['name'];
				$img=$ggg[$appid]['data']["header_image"];

				$sprice0=substr($ggg[$appid]['data']["price_overview"]["initial"],0,-2);
				$sprice1=substr($ggg[$appid]['data']["price_overview"]["initial"],-2);
				$sprice=$sprice0.'.'.$sprice1;

				$price0=substr($ggg[$appid]['data']["price_overview"]["final"],0,-2);
				$price1=substr($ggg[$appid]['data']["price_overview"]["final"],-2);
				$price=$price0.'.'.$price1;

				$discount=$ggg[$appid]['data']["price_overview"]["discount_percent"];

			}



			if(empty($img)){

				$img='assets/img/header.jpg';

			}


			if(empty($_SESSION['gid'][$appid])){

				$_SESSION['gid'][$appid]=$appid; 
				$_SESSION['name'][$appid]=$name; 
				$_SESSION['type'][$appid]=$type; 
				$_SESSION['img'][$appid]=$img; 
				$_SESSION['sprice'][$appid]=$sprice; 
				$_SESSION['price'][$appid]=$price; 
				$_SESSION['discount'][$appid]=$discount; 


			}



		}

		$_SESSION['sumbank']=0;

		foreach ($_SESSION['gid'] as $v) {

			$id=$_SESSION['gid'][$v];
			$name=$_SESSION['name'][$v];
			$type=$_SESSION['type'][$v];
			$img=$_SESSION['img'][$v];
			$price=$_SESSION['price'][$v];
			$sprice=$_SESSION['sprice'][$v];
			$discount=$_SESSION['discount'][$v];

			if($type=='package'){

				$url='http://store.steampowered.com/sub/'.$id;

			}else{

				$url='http://store.steampowered.com/app/'.$id;

			}

			?>
			<div class="media media-game">
				<div class="media-left">
					<a href="<?php echo $url;?>" target="_blank">
						<img class="media-object" src="<?php echo $img;?>" width="80">
					</a>
				</div>
				<div class="media-body">
					<p class="media-heading"><a href="<?php echo $url;?>" target="_blank"><?php echo($type=='dlc'?'[DLC]':'');?> <?php echo $name;?></a></p>
					<p><?php echo $price;?> THB</p>
				</div>
				<a href="#" class="media-close" onclick="removecart('<?php echo $id;?>')"><i class="fa fa-times" aria-hidden="true"></i></a>
			</div>

			<?php

			$_SESSION['sumbank']+=$price;


		}

		$_SESSION['total_service']=$this->service($_SESSION['sumbank']);

		?>


		<div class="media media-game">
			<div class="media-left">
				<a href="#">
					<img class="media-object" src="assets/img/steam.png" width="35">
				</a>
			</div>
			<div class="media-body">
				<p class="media-heading"><a href="#">ค่าบริการ</a></p>
				<p><?php echo $_SESSION['total_service'];?> THB ( +14.9% สำหรับบัตรทรูมันนี่ )</p>
			</div>
		</div>

		<br><br>

		<?php

		$_SESSION['total_bank']=number_format($_SESSION['sumbank']+$_SESSION['total_service']-1);
		$_SESSION['total_true']=$this->truemoney($_SESSION['sumbank'])

		?>

		<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal01">ชำระผ่านธนาคาร <?php echo $_SESSION['total_bank'];?>.xx THB</button>
		<br>
		<button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#myModal02">ชำระผ่านทรูมันนี่ <?php echo $_SESSION['total_true'];?> THB</button>

		<?php
               // session_destroy();

	}



	function removecart()
	{

		$appid=$_POST['appid'];


		unset($_SESSION['gid'][$appid]);
		unset($_SESSION['name'][$appid]);
		unset($_SESSION['type'][$appid]);
		unset($_SESSION['img'][$appid]);
		unset($_SESSION['price'][$appid]);
		unset($_SESSION['sprice'][$appid]);
		unset($_SESSION['discount'][$appid]);

		if(!empty($_SESSION['gid'])){

			$_SESSION['sumbank']=0;

			foreach ($_SESSION['gid'] as $v) {

				$id=$_SESSION['gid'][$v];
				$name=$_SESSION['name'][$v];
				$type=$_SESSION['type'][$v];
				$img=$_SESSION['img'][$v];
				$price=$_SESSION['price'][$v];
				$sprice=$_SESSION['sprice'][$v];
				$discount=$_SESSION['discount'][$v];

				if($type=='package'){

					$url='http://store.steampowered.com/sub/'.$id;

				}else{

					$url='http://store.steampowered.com/app/'.$id;

				}

				?>
				<div class="media media-game">
					<div class="media-left">
						<a href="<?php echo $url;?>" target="_blank">
							<img class="media-object" src="<?php echo $img;?>" width="80">
						</a>
					</div>
					<div class="media-body">
						<p class="media-heading"><a href="<?php echo $url;?>" target="_blank"><?php echo($type=='dlc'?'[DLC]':'');?> <?php echo $name;?></a></p>
						<p><?php echo $price;?> THB</p>
					</div>
					<a href="#" class="media-close" onclick="removecart('<?php echo $id;?>')"><i class="fa fa-times" aria-hidden="true"></i></a>
				</div>

				<?php

				$_SESSION['sumbank']+=$price;


			}

			$_SESSION['total_service']=$this->service($_SESSION['sumbank']);

			?>


			<div class="media media-game">
				<div class="media-left">
					<a href="#">
						<img class="media-object" src="assets/img/steam.png" width="35">
					</a>
				</div>
				<div class="media-body">
					<p class="media-heading"><a href="#">ค่าบริการ</a></p>
					<p><?php echo $_SESSION['total_service'];?> THB ( +14.9% สำหรับบัตรทรูมันนี่ )</p>
				</div>
			</div>

			<br><br>

			<?php

			$_SESSION['total_bank']=number_format($_SESSION['sumbank']+$_SESSION['total_service']-1);
			$_SESSION['total_true']=$this->truemoney($_SESSION['sumbank'])

			?>

			<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal01">ชำระผ่านธนาคาร <?php echo $_SESSION['total_bank'];?>.xx THB</button>
			<br>
			<button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#myModal02">ชำระผ่านทรูมันนี่ <?php echo $_SESSION['total_true'];?> THB</button>


			<?php

		}else{


			echo '<div class="text-center">ไม่มีสินค้าในตะกร้า</div>';

		}

	}




	public function service($value){

		if($value >= 1 && $value <= 60){

			return '5.00';

		}else if($value >= 61 && $value <= 150){

			return '10.00';

		}else if($value >= 151 && $value <= 300){

			return '15.00';

		}else if($value >= 301 && $value <= 500){

			return '30.00';

		}else if($value >= 501 && $value <= 1000){

			return '50.00';

		}else if($value >= 1001 && $value <= 2000){

			return '75.00';

		}else{

			return '99.00';

		}


	}


	public function truemoney($value){

		$sum = $value*14.9/100;

		return number_format($sum+$value);

	}







}

?>
