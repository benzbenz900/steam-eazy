<?php

$lang['jb_blankpage_message'] = "ขั้นตอนการอัปโหลดไม่ได้เริ่มต้นหรือเริ่มต้น แต่ก็ยังไม่เสร็จหรือเบราว์เซอร์ของคุณไม่สามารถเข้าถึงเซิร์ฟเวอร์ระยะไกล";

/* End of file jbstrings_lang.php */
/* Location: ./application/language/english/jbstrings_lang.php */
