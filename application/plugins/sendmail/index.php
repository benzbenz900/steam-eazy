<?php
session_start();

if(isset($_POST['send_mail'])){

define('__host__smtp__', 'localhost'); // Protocall แนะนำ Gmail เพราะ Dkim แรงที่สุด
define('__ssl__tls__', ''); // เข้ารหัสอีเมล แนะนำ SSL
define('__port__', '25'); // Port การส่งของ Protocall Host

define('__username__', 'no-reply@prerunner.co.th'); // ชื้อผู้ใช้งาน smtp
define('__password__', '12345678'); // รหัสผ่าน smtp

define('__form__mail__', 'no-reply@prerunner.co.th'); // อีเมลที่แสดงในการส่ง แนะนำให้ใช้จาก __username__ เพราะ ระบบเมลบ้างตัวอาจตัดลงถัง
define('__reply__mail__', 'no-reply@prerunner.co.th'); // อีเมลที่ใช้การในให้ลูกค้าตอบกลับมา แนะนำให้ใช้จาก __username__

require_once('class.phpmailer.php');

for ($i=0; $i < 2 ; $i++) { 

	$mail = new PHPMailer();
	$mail->IsHTML(true);
	$mail->IsSMTP();
	$mail->CharSet = 'UTF-8';
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = __ssl__tls__;
	$mail->Host = __host__smtp__;
	$mail->Port = __port__;
	$mail->Username = __username__;
	$mail->Password = __password__;
	$mail->From = __form__mail__;
	$mail->AddReplyTo = __reply__mail__;


	if($i==0){

	$mail->FromName = $_POST['name'];
	$mail->Subject = $_POST['subject']; 

    $messages .= $_POST['message'];
    $messages .= "<br/><br/><hr /> <p>ผู้ส่ง : ".$_POST['name']."</p>";
    $messages .= "<p>โทรศัพท์ : ".$_POST['telephone']."</p>";
    $messages .= "<p>อีเมล์ : ".$_POST['email']."</p>";
	$mail->Body = $messages;
	
	$mail->AddAddress('prn_forklift@hotmail.co.th');
	$mail->AddCC( 'no-reply@prerunner.co.th');
	$mail->set('X-Priority', '1');
	$mail->Send();
	$mail->SmtpClose();

    }else{

    $mail->FromName = 'Prerunner.co.th';
    $mail->Subject = 'Prerunner ได้รับอีเมล์ของท่านแล้ว';	

    $messages2 .= "สวัสดีคุณ ".$_POST['name']."<br/>ทาง  Prerunner.co.th  ได้รับอีเมล์ของท่านแล้ว <br/> ขอบคุณที่ใช้บริการของเรา<br/><br/>";
    $messages2 .= "<hr /> <p>https://www.prerunner.co.th </p>";
    $messages2 .= "<p>โทรศัพท์ : 02-101-1604 / 085-903-1900</p>";
    $messages2 .= "<p>อีเมล์ : prn_forklift@hotmail.co.th</p>";

    $mail->Body = $messages2;
	
	$mail->AddAddress($_POST['email']);
	$mail->AddCC( 'no-reply@prerunner.co.th');
	$mail->set('X-Priority', '1');
	$mail->Send();
	$mail->SmtpClose();
    }

 }

	header( "location: https://www.prerunner.co.th/" );

	$_SESSION['email']='success';

}else{

	header( "location: https://www.prerunner.co.th/" );

	$_SESSION['email']='fail';

}
