<?php

date_default_timezone_set("Asia/Bangkok");


function pip()
{
	global $config;

    // Set our defaults
	$controller = $config['default_controller'];
	$action = 'index';
	$url = '';

	
	// Get request url and script url
	$request_url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
	$script_url  = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

	// Get our url path and trim the / of the left and the right
	if($request_url != $script_url) $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $script_url)) .'/', '', $request_url, 1), '/');

	// Split the url into segments
	$segments = explode('/', $url);
	// echo $script_url;
	
	// Do our default checks
	if(isset($segments[0]) && $segments[0] != '') $controller = $segments[0];
	if(isset($segments[1]) && strstr($segments[1],"?")){
		if(isset($segments[1]) && $segments[1] != '') $action = 'truepay';
	}else{
		if(isset($segments[1]) && $segments[1] != '') $action = $segments[1];
	}

	// Get our controller file
	$path = APP_DIR . 'controllers/' . $controller . '.php';

	if(file_exists($path)){

		require_once($path);

	} else {

		$controller = $config['error_controller'];
		require_once(APP_DIR . 'controllers/' . $controller . '.php');
	}

    // Check the action exists
	if(!method_exists($controller, $action)){

		$controller = $config['error_controller'];

		require_once(APP_DIR . 'controllers/' . $controller . '.php');

		$action = 'index';
	}
	
	// Create object and call method
	$obj = new $controller;
	
	die(call_user_func_array(array($obj, $action), array_slice($segments, 2)));
}


function servicechage($value){

	if($value >= 1 && $value <= 60){

		return '5.00';

	}else if($value >= 61 && $value <= 150){

		return '10.00';

	}else if($value >= 151 && $value <= 300){

		return '15.00';

	}else if($value >= 301 && $value <= 500){

		return '30.00';

	}else if($value >= 501 && $value <= 1000){

		return '50.00';

	}else if($value >= 1001 && $value <= 2000){

		return '75.00';

	}else{

		return '99.00';

	}


}


function truechage($value){

	$sum = $value*14.9/100;

	return number_format($sum+$value);

}



?>
